import { Component , HostBinding} from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { getBackendUrl } from '../../shared/utils/env'

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  animations: [
    // animation triggers go here
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('900ms ease-in', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('500ms ease-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LandingComponent {

  searchShown = false
  isMobile = false
  hovered = false;

  cardText = `Over 5,000 standardized financial, 
    supplemental, segment, ratio, and industry-specific 
    data items on an annual, quarterly, semi-annual, 
    last twelve month and year-to-date basis`

  t1r1 = ''  
  t2r1 = '' 
  t3r1 = '' 

  t1r2 = '' 
  t1r3 = '' 


  trendingDataSets = [
    {
      title : 'Job Postings',
      image :  'card1.jpg'
    },
    {
      title : 'Machine Learning',
      image :  'card5.jpg'
    },
    {
      title : 'Industrial Data',
      image :  'card1.jpg'
    }
  ]

  recommendedDataSets = [
   
    {
      title : 'Industrial Data',
      image :  'card2.jpg'
    },
    {
      title : 'Carrier Data',
      image :  'card1.jpg'
    },
    {
      title : 'Job Library',
      image :  'card4.jpg'
    },
    
  ]

  allDataSets = [
    {
      title : 'Job Postings',
      image :  'card1.jpg'
    },
    {
      title : 'Machine Learning',
      image :  'card5.jpg'
    },
    {
      title : 'Industrial Data',
      image :  'card2.jpg'
    },
    {
      title : 'Carrier Data',
      image :  'card1.jpg'
    },
    {
      title : 'Job Library',
      image :  'card4.jpg'
    },
    {
      title : 'Mercer Post',
      image :  'card3.jpg'
    },
    {
      title : 'Postings',
      image :  'card2.jpg'
    },
    {
      title : 'Job Postings',
      image :  'card1.jpg'
    },
    {
      title : 'New Labels',
      image :  'card5.jpg'
    }
  ]

  deviceInfo : any = null;

  constructor(private deviceService: DeviceDetectorService) {
    this.epicFunction();
    console.log(getBackendUrl())
  }

  epicFunction() {
    this.isMobile = this.deviceService.isMobile() || this.deviceService.isTablet();
  }

  updateCards(offset : number){
    let limit = offset + 3;
    this.trendingDataSets = []
    for (offset; offset < limit ; offset++) {
      console.log(offset , limit)
      this.trendingDataSets.push(this.allDataSets[offset])
    }
  }

  updateCards2(offset : number){
    let limit = offset + 3;
    this.recommendedDataSets = []
    for (offset; offset < limit ; offset++) {
      console.log(offset , limit)
      this.recommendedDataSets.push(this.allDataSets[offset])
    }
  }
}
