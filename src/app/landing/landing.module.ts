import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingRoutingModule } from './landing.routing.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { LandingComponent } from './info/landing.component'
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { DetailsComponent } from '../landing/details/details.component'
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [LandingComponent,DetailsComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    CarouselModule,
    MatCheckboxModule,
    SharedModule
  ]
})
export class LandingModule { }
