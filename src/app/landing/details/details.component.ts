import { Component , HostBinding} from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  animations: [
    // animation triggers go here
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('900ms ease-in', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('500ms ease-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class DetailsComponent {

  searchShown = false
  isMobile = false
  hovered = false;

  cardText = 'Complete BNB chain dataset of blocks, transactions, token transfers, tokens, logs, receipts, and contracts.'

  deviceInfo : any = null;

  constructor(private deviceService: DeviceDetectorService) {
    this.epicFunction();
  }

  epicFunction() {
    this.isMobile = this.deviceService.isMobile() || this.deviceService.isTablet();
  }
}
