import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CustomCardComponent } from './components/card/custom-card.component';
import { CustomCardSmallComponent } from './components/card-small/custom-card-small.component'
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  imports : [CommonModule,MatButtonModule],
  declarations: [
      CustomCardComponent,
      CustomCardSmallComponent
    ],
  exports : [
    CustomCardComponent,
    CustomCardSmallComponent
  ]
})
export class SharedModule { }
