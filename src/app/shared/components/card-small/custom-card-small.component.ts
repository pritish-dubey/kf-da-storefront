import { Component , Input , HostBinding} from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'custom-card-small',
  templateUrl: './custom-card-small.component.html',
  styleUrls: ['./custom-card-small.component.scss'],
  animations: [
    // animation triggers go here
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('500ms ease-in', style({ opacity: 1 })),
      ]),
      // transition(':leave', [
      //   animate('100ms', style({ opacity: 0 }))
      // ])
    ])
  ]
})
export class CustomCardSmallComponent {



  hovered = false;
  @Input() id = 100
  @Input() title = ''
  @Input() text = ''
  @Input() bgImage = ''
}
